import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(name='flask_sqlaclhemy_webquery',
      version='0.1',
      description='Query SQLAlchemy models through a web interface',
      long_description=read('README.md'),
      url='https://gitlab.com/gergelypolonkai/flask-sqlalchemy-webquery',
      author='Gergely Polonkai',
      author_email='gergely@polonkai.eu',
      licensce='BSD',
      packages=['flask_sqlalchemy_webquery'],
      install_requires=[
          'flask-sqlalchemy',
          'flask-bootstrap',
      ],
      platforms='any',
      include_package_data=True,
      classifiers=[
          'Environment :: Web Environment',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: BSD License',
          'Operating System :: OS Independent',
          'Programming Language :: Python',
          'Programming Language :: Python :: 2',
          'Programming Language :: Python :: 3',
          'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
          'Topic :: Software Development :: Libraries :: Python Modules',
      ],
      zip_safe=False)
