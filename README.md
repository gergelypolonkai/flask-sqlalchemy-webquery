# Flask-SQLAlchemy-WebQuery

Build SQLAlchemy queries through a web interface.

This is a generic web interface to query your SQLAlchemy models, and save the results in a CSV file.

This is a work in progress.  Feel free to contribute!

# License

This library is licensed under the BSD 3 Clause license.  See the LICENSE file for details.

# Contributing

If you have an idea, open an issue on GitLab.
